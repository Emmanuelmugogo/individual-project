<%-- 
    Document   : Home
    Created on : May 14, 2016, 8:58:50 AM
    Author     : Emmanuelmugogo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/Home">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/ContactPage">Contact</a></li>
                            <li><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li class="active"><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                            </li> 

                        </ul>
<!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                            <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                                                        </div>
                            
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                            <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                                                        </div>
                            Welcome <sec:authentication property="principal.username" />
                            <button method="LINK" action="${pageContext.request.contextPath}/j_spring_security_logout" type="submit" class="btn btn-primary">Log Out</button>
                        </form>-->

                    </div>
                </div>
            </nav>
            <br>

            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <center><h3>Personal Information</h3></center>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="edit-firstName" class="col-sm-3 control-label">First Name:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="edit-firstName" placeholder="First Name">
                                    </div>

                                    <label for="edit-lastName" class="col-sm-3 control-label">Last Name:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="edit-lastName" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-email" class="col-sm-3 control-label">Email:</label>
                                    <div class="col-sm-3">
                                        <input type="email" class="form-control" id="edit-email" placeholder="Email">
                                    </div>

                                    <label for="edit-phone" class="col-sm-3 control-label">Phone:</label>
                                    <div class="col-sm-3">
                                        <input type="tel" class="form-control" id="edit-phone" placeholder="Phone #">
                                    </div>
                                </div>            
                                <div class="form-group">
                                    <label for="edit-address" class="col-sm-3 control-label">Address:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="edit-address">
                                    </div>
                                </div>            
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-primary" id="update-userInformation-button">Update Information</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>




                    <div class="col-xs-12 col-md-6">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <center><h3>Update Password</h3></center>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="form-horizontal">

                                        <div class="form-group">
                                            <label for="edit-password" class="col-sm-4 control-label">New Password:</label>
                                            <div class="col-sm-6">
                                                <input type="password" class="form-control" id="edit-password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="edit-password" class="col-sm-4 control-label">Repeat Password:</label>
                                            <div class="col-sm-6">
                                                <input type="password" class="form-control" id="edit-password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-6">
                                                <button type="submit" class="btn btn-primary" id="update-password-button">Update Password</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
