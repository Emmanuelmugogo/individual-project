<%-- 
    Document   : Home
    Created on : May 14, 2016, 8:58:50 AM
    Author     : Emmanuelmugogo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/Home">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/Contact">Contact</a></li>
                            <li><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li class="active"><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                            </li> 

                        </ul>
                        <!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                                                    <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                                                                                </div>
                                                    
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                                                    <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                                                                                </div>
                                                    Welcome <sec:authentication property="principal.username" />
                                                    <button method="LINK" action="${pageContext.request.contextPath}/j_spring_security_logout" type="submit" class="btn btn-primary">Log Out</button>
                                                </form>-->

                    </div>
                </div>
            </nav>
            <br><br>
            <h3>Current pending work & status</h3>
            <div id="statusTableDiv">

                <table id="statusTable" class="table table-striped">
                    <tr>
                        <th width="20%">Work Order Id</th>
                        <th width="20%">VIN</th>
                        <th width="20%">Status</th>
                        <th width="20%">ETA Completion</th>

                    </tr>
                    <!--
                     #3: This holds the list of Work status - we will add rows
                    dynamically
                     using jQuery
                    -->
                    <tbody id="contentRows1"></tbody>
                </table>
            </div>
            <br>

            <h3>Your Personalized Happy Hotrod's Work & Repair History</h3>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dapibus, enim vitae aliquam sagittis, 
                velit diam mattis mi, in mattis ligula purus ut nulla. Fusce ut nisl ac odio finibus rutrum sed et odio. 
                Duis sodales at elit vitae sodales. Aliquam sollicitudin mattis lectus sit amet sagittis. Sed ultrices diam
                a dui eleifend, non sagittis ex suscipit.
            </p>
            <br>
            <div id="historyTableDiv">

                <table id="historyTable" class="table table-striped">
                    <tr>
                        <th width="30%">Work Order Id</th>
                        <th width="30%">VIN</th>
                        <th width="20%">Service Date</th>

                    </tr>
                    <%--        <c:forEach var="orderRecord" items="${orderRecord}">
                                    <c:choose>
                                        <c:when test="${orderRecord.orderStatus.statusId == 8}">
                                            <tr> 
                                                <td>${orderRecord.workOrder.orderId}</td>
                                                <td>${orderRecord.workOrder.vehicle.vin}</td>
                                                <td>${orderRecord.recordDate}</td>
                                            </tr>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>--%>
                    <!--
                     #3: This holds the list of Work History - we will add rows
                    dynamically
                     using jQuery
                    -->
                    <tbody id="contentRows2"></tbody>
                </table>
            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/workOrder.js"></script>
    </body>
</html>
