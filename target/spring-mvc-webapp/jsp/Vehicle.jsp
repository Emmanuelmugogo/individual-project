<%-- 
    Document   : Home
    Created on : May 14, 2016, 8:58:50 AM
    Author     : Emmanuelmugogo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/Home">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/Contact">Contact</a></li>
                            <li><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                            </li> 

                        </ul>
                        <!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                                                    <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                                                                                </div>
                                                    
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                                                    <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                                                                                </div>
                                                    Welcome <sec:authentication property="principal.username" />
                                                    <button method="LINK" action="${pageContext.request.contextPath}/j_spring_security_logout" type="submit" class="btn btn-primary">Log Out</button>
                                                </form>-->

                    </div>
                </div>
            </nav>
            <br>
            <h3>Happy Hotrod - Vehicle Repair History</h3>

            <ul>
                <li>Vin# - </li>
                <li>Year - </li>
                <li>Make - </li>
                <li>Model - </li>
                <li>Color - </li>
            </ul>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Quisque finibus risus ut arcu molestie scelerisque. Ut nulla elit, 
                bibendum vel porttitor eget, lobortis non tellus. Nunc cursus tristique 
                nibh ac pellentesque. Aenean nisi dui, fringilla ut urna ut, tristique 
                euismod risus. Phasellus quis dui at nisl tristique semper.
            </p>

            <br>
            <div id="vehicleInfoTableDiv">

                <table id="vehicleInfoTable" class="table table-striped">
                    <tr>
                        <th width="20%">Work Order Id</th>
                        <th width="15%">Service Date</th>
                        <th width="25%">Notes</th>

                    </tr>

                    <tbody id="contentRows"></tbody>
                </table>
            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
