/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('#create-order-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'vehicle',
            data: JSON.stringify({
//                customerId: $('#add-customerId').val(),
                vin: $('#add-vin').val(),
                make: $('#add-make').val(),
                year: $('#add-year').val(),
                model: $('#add-model').val(),
                color: $('#add-color').val(),
                notes: $('#add-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
//            $('#add-cutomerId').val('');
            $('#add-vin').val('');
            $('#add-make').val('');
            $('#add-year').val('');
            $('#add-model').val('');
            $('#add-color').val('');
            $('#add-notes').val('');


        });
    });
});