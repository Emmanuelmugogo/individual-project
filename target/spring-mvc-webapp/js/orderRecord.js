/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    loadOrderRecords();

});

// on click for our search button
$('#search-orderRecord-button').click(function (event) {
    // we don’t want the button to actually submit
    // we'll handle data submission via ajax
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/orderRecords',
        data: JSON.stringify({
            orderId: $('#search-orderId').val(),
            vin: $('#search-vin').val(),
            firstName: $('#search-customer').val(),
            lastName: $('#search-worker').val(),
            status: $('#search-status').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#search-orderId').val('');
        $('#search-vin').val('');
        $('#search-customer').val('');
        $('#search-worker').val('');
        $('#search-status').val('');
        
        fillOrderRecordsTable(data, status);
    });
});

function loadOrderRecords() {

    $.ajax({
        url: 'orderRecords'
    }).success(function (data, status) {
        fillOrderRecordsTable(data, status);
    });

}

function clearOrderRecordsTable()
{
    $("#contentRows").empty();
}

function updateOrderRecord() {

}

function fillOrderRecordsTable(orderRecordList, status) {
    
    clearOrderRecordsTable();

    var orTable = $('#contentRows');

        $.each(orderRecordList, function (index, orderRecord) {
            orTable.append($('<tr>')
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-workOrder-id': orderRecord.workOrder.orderId,
                                'data-toggle': 'modal',
                                'data-target': '#editModal'
                            }).text(orderRecord.workOrder.orderId)))
                    .append($('<td>').text(orderRecord.workOrder.vehicle.vin))
                    .append($('<td>').text(orderRecord.user.firstName + " " + orderRecord.user.lastName))
                    .append($('<td>').text(orderRecord.user.firstName + " " + orderRecord.user.lastName))
                    .append($('<td>').text(orderRecord.orderStatus.status))
                    .append($('<td>').text(orderRecord.recordDate))
                    .append($('<td>').text(orderRecord.etaDate))
                    .append($('<td>').append($('<a>').attr({'onclick': 'updateOrderRecord(' + orderRecord.workOrder.orderId + ')'}).text('Update')))
                    );
        });

   
    
}

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var orderId = element.data('workOrder-id');

    var modal = $(this);

    $.ajax({
        url: 'orderRecord/' + orderId
    }).success(function (orderRecord) {
        //use .text on text display fields like <h3> or <span>
        modal.find('#workOrder-id').text(orderRecord.workOrder.orderId);
        //use .val on input fields
        modal.find('#edit-vin').val(orderRecord.workOrder.vehicle.vin);
        modal.find('#edit-customer').val(orderRecord.user.firstName + " " + orderRecord.user.lastName);
        modal.find('#edit-customerId').val(orderRecord.user.userId);
        modal.find('#edit-assignedTo').val(orderRecord.user.lastName);
        modal.find('#edit-status').val(orderRecord.orderStatus.status);
        modal.find('#edit-currentETA').val(orderRecord.etaDate);
    });


});
