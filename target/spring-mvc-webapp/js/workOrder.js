$(document).ready(function () {

    loadPendingWorkOrders();
    loadWorkOrderHistory();

});





function loadWorkOrderHistory() {
    
    clearHistoryTable();

    var hTable = $('#contentRows2');

    $.ajax({
        url: 'orderRecords'
    }).success(function (data, status) {
        $.each(data, function (index, orderRecord) {
            if (orderRecord.orderStatus.statusId === 8) {
            hTable.append($('<tr>')
                    .append($('<td>').text(orderRecord.workOrder.orderId))
                    .append($('<td>').text(orderRecord.workOrder.vehicle.vin))
                    .append($('<td>').text(orderRecord.recordDate)));

            }
        });
    });
}

function clearHistoryTable() {
    $('#contentRows2').empty();
}

function loadPendingWorkOrders() {
    clearPendingWorkOrderTable();

    var pwoTable = $('#contentRows1');

    $.ajax({
        url: 'orderRecords'
    }).success(function (data, status) {
        $.each(data, function (index, orderRecord) {
            if (orderRecord.orderStatus.statusId < 8) {
            pwoTable.append($('<tr>')
                    .append($('<td>').text(orderRecord.workOrder.orderId))
                    .append($('<td>').text(orderRecord.workOrder.vehicle.vin))
                    .append($('<td>').text(orderRecord.orderStatus.status))
                    .append($('<td>').text(orderRecord.etaDate)));

            }
        });
    });
}

function clearPendingWorkOrderTable() {
    $('#contentRows1').empty();
}


