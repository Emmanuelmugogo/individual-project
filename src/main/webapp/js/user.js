/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadUsers();
    
    $('#add-user-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'user',
            data: JSON.stringify({
                userName: $('#userName').val(),
                firstName: $('#firstName').val(),
                lastName: $('#lastName').val(),
                password: $('#passWord').val(),
                phone: $('#phone').val(),
                email: $('#email').val(),
                address: $('#address').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#userName').val('');
            $('#firstName').val('');
            $('#lastName').val('');
            $('#passWord').val('');
            $('#phone').val('');
            $('#email').val('');
            $('#address').val('');

            loadUsers();
        });
    });
    
    
    
    $('#update-userInformation-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'user/' + $('#edit-user-id').val(), //how to get the logged in user Id?
            data: JSON.stringify({
                firstName: $('#edit-firstName').val(),
                lastName: $('#edit-lastName').val(),
                email: $('#edit-email').val(),
                phone: $('#edit-phone').val(),
                address: $('#edit-address').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {

            loadUsers();
        });
    });
    
    
    $('#update-password-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'user/' + $('#edit-user-id').val(), //how to get the logged in user Id?
            data: JSON.stringify({
                password: $('#edit-password').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {

            loadUsers();
        });
    });
});

function loadUsers() {
    $.ajax({
        url: "users"
    }).success(function (data, status) {
        fillUsersTable(data, status);
    });
}


function fillUsersTable(userList, status) {

    clearUserTable();

    var uTable = $('#contentRows');

    $.each(userList, function (index, user) {
        uTable.append($('<tr>')
                .append($('<td>').text(user.userId))
                .append($('<td>').text(user.userName))
                .append($('<td>').text(user.userName))   //supposed to be .append($('<td>').text(user.authority.userRole)) BUT WHY NOT WORKING..???
                .append($('<td>').append($('<a>').attr({'onclick': 'deleteUser(' + user.userId + ')'}).text('Delete')))
                .append($('<td>').append($('<a>').attr({'onclick': 'resetPassword(' + user.userId + ')'}).text('Reset Password')))
                );
    });



}

function clearUserTable()
{
    $("#contentRows").empty();
}

function deleteUser(id) {
    var answer = confirm("Do you really want to delete this user?");

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'user/' + id
        }).success(function () {
            loadUsers();
        });
    }
}

function resetPassword(id) {
    var answer = confirm("Do you really want to reset the password for this user");

    if (answer === true) {
        $.ajax({
            type: 'PUT',
            url: 'user/' + id
        }).success(function (data, status) {
            $('#passWord').val('happy-hotrods');
            loadUsers();
        });
    }
}



$('#search-user-button').click(function (event) {

    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/users',
        data: JSON.stringify({
            firstName: $('#search-userId').val(),
            lastName: $('#search-userName').val()

        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#search-userId').val('');
        $('#search-userName').val('');

        fillUsersTable(data, status);
    });
});