<%-- 
    Document   : Home
    Created on : May 14, 2016, 8:58:50 AM
    Author     : Emmanuelmugogo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/Home">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/Contact">Contact</a></li>
                            <li><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li class="active"><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                            </li> 

                        </ul>
                        <!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                                                    <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                                                                                </div>
                                                    
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                                                    <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                                                                                </div>
                                                    Welcome <sec:authentication property="principal.username" />
                                                    <button method="LINK" action="${pageContext.request.contextPath}/j_spring_security_logout" type="submit" class="btn btn-primary">Log Out</button>
                                                </form>-->

                    </div>
                </div>
            </nav>
            <br><br>
            <h3>Happy HotRods Directory</h3>
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Search</h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <form class="form-horizontal">
                                <div class="form-group">

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="search-userId" placeholder="User Id">
                                    </div>

                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="search-userName" placeholder="Name">
                                    </div>
                                    <button type="submit" id="search-user-button" class="btn btn-primary">Search</button>

                                </div>

                            </form>
                        </div>

                        <div id="membersTableDiv">

                            <table id="membersTable" class="table table-striped">
                                <tr>
                                    <th width="5%">User Id</th>
                                    <th width="25%">Name</th>
                                    <th width="20%">Type</th>

                                </tr>
                                <!--
                                 #3: This holds the list of members list - we will add rows
                                dynamically
                                 using jQuery
                                -->
                                <tbody id="contentRows"></tbody>
                            </table>
                        </div>
                    </div>



                    <div class="col-xs-12 col-md-6">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Add User</h3>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <form class="form-horizontal">

                                <div class="form-group">
                                    <label for="userName" class="col-sm-4 control-label">User Name:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="userName" placeholder="User Name">
                                    </div>


                                    <input type="checkbox" id="isEmployee"> Employee?

                                </div>
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-4 control-label">First Name</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="firstName" placeholder="First Name">                                    
                                    </div>

                                    <input type="checkbox" id="isAdmin"> Admin?

                                </div>
                                <div class="form-group">
                                    <label for="lastName" class="col-sm-4 control-label">Last Name</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="lastName" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="passWord" class="col-sm-4 control-label">Password</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="passWord" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-4 control-label">Phone</label>
                                    <div class="col-sm-4">
                                        <input type="tel" class="form-control" id="phone" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-4">
                                        <input type="email" class="form-control" id="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-sm-4 control-label">Address</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <button type="submit" id="add-user-button" class="btn btn-primary">Add User</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/user.js"></script>

    </body>
</html>
