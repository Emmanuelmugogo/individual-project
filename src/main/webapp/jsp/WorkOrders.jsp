<%-- 
    Document   : Home
    Created on : May 14, 2016, 8:58:50 AM
    Author     : Emmanuelmugogo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/Home">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/Contact">Contact</a></li>
                            <li class="active"><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                            </li> 

                        </ul>
                        <!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                                                    <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                                                                                </div>
                                                    
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                                                    <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                                                                                </div>
                                                    Welcome <sec:authentication property="principal.username" />
                                                    <button method="LINK" action="${pageContext.request.contextPath}/j_spring_security_logout" type="submit" class="btn btn-primary">Log Out</button>
                                                </form>-->

                    </div>
                </div>
            </nav>
            <br>
            <h3>Our Happy Work Orders</h3>
            <hr>
            <h3>Search</h3>
            <br>
            <div class="row">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="workOrder#" class="col-sm-0 control-label"></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="search-orderId" placeholder="Work Order #">
                        </div>

                        <label for="vin" class="col-sm-0 control-label"></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="search-vin" placeholder="Vin #">
                        </div>
                        <label for="customer" class="col-sm-0 control-label"></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="search-customer" placeholder="Customer">
                        </div>
                        <label for="Worker" class="col-sm-0 control-label"></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="search-worker" placeholder="Worker">
                        </div>
                        <label for="status" class="col-sm-0 control-label"></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="search-status" placeholder="Status">
                        </div>

                        <button type="submit" id="search-orderRecord-button" class="btn btn-primary">Search</button>

                    </div>
                </form>
            </div>
            <hr>
            <div id="workOrderTableDiv">

                <table id="workOrderTable" class="table table-striped">
                    <tr>
                        <th width="10%">Work Order Id</th>
                        <th width="15%">VIN</th>
                        <th width="15%">Customer</th>
                        <th width="15%">Assigned To</th>
                        <th width="15%">Status</th>
                        <th width="10%">Date Created</th>
                        <th width="10%">ETA</th>
                    </tr>
                    <!--
                     #3: This holds the list of Work Orders - we will add rows
                    dynamically
                     using jQuery
                    -->
                    <tbody id="contentRows"></tbody>
                </table>
            </div>
        </div>


        <!--edit modal-->


        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>

                        <h4 class="modal-title" id="detailsModalLabel">Work Order # </h4><h3 id="workOrder-id"></h3>  
                    </div>

                    <div class="modal-body">
                        <h3 id="workOrder-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Vin #</th>
                                <td id="edit-vin"></td>
                            </tr>
                            <tr>
                                <th>Customer:</th>
                                <td id="edit-customer"></td>
                            </tr>
                            <tr>
                                <th>Customer Id:</th>
                                <td id="edit-customerId"></td>
                            </tr>
                            <tr>
                                <th>Currently Assigned:</th>
                                <td id="edit-assignedTo"></td>
                            </tr>
                            <tr>
                                <th>Current Status:</th>
                                <td id="edit-status"></td>
                            </tr>
                            <tr>
                                <th>Current ETA:</th>
                                <td id="edit-currentETA"></td>
                            </tr>
                        </table>
                        <hr>

                        <!--<div class="container">-->

                        <div class="row">
                            <div class="col-xs-12 col-md-6">

                                <div class="row">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="assignedTo" class="col-sm-2 control-label">Assigned To:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="assignedTo" placeholder="Employee">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ETA" class="col-sm-2 control-label">ETA</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="ETA" placeholder="ETA">
                                            </div>
                                        </div>

                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Select Status
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#">Awaiting Evaluation</a></li>
                                                <li><a href="#">Under Evaluation</a></li>
                                                <li><a href="#">Awaiting C Approval</a></li>
                                                <li><a href="#">Waiting for Parts</a></li>
                                                <li><a href="#">In Repairs</a></li>
                                                <li><a href="#">Awaiting Pickup</a></li>
                                                <li><a href="#">Closed</a></li>
                                                <!--                                                        <li role="separator" class="divider"></li>
                                                                                                        <li><a href="#">Separated link</a></li>-->
                                            </ul>
                                        </div>



                                    </form>
                                </div>
                            </div>



                            <div class="col-xs-12 col-md-6">

                                <div class="row">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="notes" class="col-sm-4 control-label">Extra Notes:</label>

                                        </div>
                                        <div class="col-sm-offset-1 col-sm-10">
                                            <input type="text" class="form-control" id="edit-notes">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!--</div>-->


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="update-button" class="btn btn-success" data-dismiss="modal">Update</button>
                    </div>
                </div>
            </div>
        </div>






        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/orderRecord.js"></script>        
    </body>
</html>
