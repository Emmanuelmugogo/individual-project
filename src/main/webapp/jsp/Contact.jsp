


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/Home">Home <span class="sr-only">(current)</span></a></li>
                            <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/Contact">Contact</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>

                        </ul>
<!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                            </div>

                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>-->

                    </div>
                </div>
            </nav>
            <br><br>
            <h2>Contact Information</h2>
            <hr>

            <h3>  Happy Hot Rods Automotive Parts & Repair</h3> 
            <p>
                4300 SW Guild Road<br>
                Columbus, OH 43000<br>
                (614) 123-1234<br>
                <br>
                Our Hours: <br>
                M-F 7:30 AM - 6:00 PM<br>
                SAT 7:30 AM to 4:00 PM<br>
                SUN Closed

            </p>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>

