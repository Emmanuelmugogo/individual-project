<%-- 
    Document   : Home
    Created on : May 14, 2016, 8:58:50 AM
    Author     : Emmanuelmugogo
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <br><br>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/MainAjaxPage">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/ContactPage">Contact</a></li>
                            <li><a href="${pageContext.request.contextPath}/WorkOrders">Work Orders</a></li>
                            <li><a href="${pageContext.request.contextPath}/Status">History & Status</a></li>
                            <li><a href="${pageContext.request.contextPath}/Members">Members</a></li>
                            <li class="active"><a href="${pageContext.request.contextPath}/Admin">Admin</a></li>
                            <li><a href="${pageContext.request.contextPath}/Profile">Profile</a></li>
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a>
                            </li> 

                        </ul>
<!--                        <form id="signin" class="navbar-form navbar-right" role="form">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                            <input id="username" type="text" class="form-control" name="username" value="" placeholder="User Name">                                        
                                                        </div>
                            
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                            <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                                        
                                                        </div>
                            Welcome <sec:authentication property="principal.username" />
                            <button method="LINK" action="${pageContext.request.contextPath}/j_spring_security_logout" type="submit" class="btn btn-primary">Log Out</button>
                        </form>-->

                    </div>
                </div>
            </nav>
            <br><br>
            <h3>Happy HotRod - Admin Page</h3>
            <div class="container">

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>New Work Order</h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="add-customerId" class="col-sm-3 control-label">Customer Id:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="add-customerId">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="add-vin" class="col-sm-3 control-label">Vin #</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="add-vin">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> New Vehicle?
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="add-make" class="col-sm-3 control-label">Make:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="add-make" placeholder="Make">
                                    </div>

                                    <label for="add-year" class="col-sm-3 control-label">Year:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="add-year" placeholder="Year">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="add-model" class="col-sm-3 control-label">Model:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="add-model" placeholder="Model">
                                    </div>

                                    <label for="add-color" class="col-sm-3 control-label">Color:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="add-color" placeholder="Color">
                                    </div>
                                </div>            
                                <div class="form-group">
                                    <label for="add-notes" class="col-sm-3 control-label">Notes:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="add-notes">
                                    </div>
                                </div>            


                            </form>
                        </div>
                    </div>


                     
                    <div class="col-xs-12 col-md-6">
                        <div class="page-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Cont...</h3>
                                </div>
                            </div>
                        </div>
                       
                   

                        <div class="row">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="reasons" class="col-sm-4 control-label">Reasons for Visit:</label>

                                </div>
                                <div class="col-sm-offset-1 col-sm-10">
                                    <input type="text" class="form-control" id="reasons">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br>
                <div>
                    <div class="col-sm-offset-5 col-sm-12">
                        <button type="submit" class="btn btn-primary" id="create-order-button">Create Order</button>
                    </div>
                </div>
            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vehicleRecord.js"></script> 
    </body>
</html>
