<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Happy Hot Rods</title>
        <style>
            .error {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #a94442;
                background-color: #f2dede;
                border-color: #ebccd1;
            }

            .msg {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #31708f;
                background-color: #d9edf7;
                border-color: #bce8f1;
            }

            #login-box {
                width: 300px;
                padding: 20px;
                margin: 100px auto;
                background: #fff;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border: 1px solid #000;
            }
        </style>
    </head>
    <body>
        <!--        <div>
                    <h2>Sign In to Happy Hot Rods App</h2>
                     #1 - If login_error == 1 then there was a failed login attempt 
                     so display an error message 
        <c:if test="${param.login_error == 1}">
            <h3>Wrong id or password!</h3>
        </c:if>
         #2 - Post to Spring security to check our authentication 
        <form method="post" class="signin" action="j_spring_security_check">
            <fieldset>
                <table cellspacing="0">
                    <tr>
                        <th>
                            <label for="username">Username
                            </label>
                        </th>

                        <td><input id="username_or_email"
                                   name="j_username"
                                   type="text" />
                        </td>
                    </tr>
                    <tr>
                        <th><label for="password">Password</label></th>
                         #2b - must be j_password for Spring 
                        <td><input id="password"
                                   name="j_password"
                                   type="password" />
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><input name="commit" type="submit" value="Sign In" /></td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>-->
        <br><br><br>
    <center><h1>Sign In to Happy Hot Rods App</h1></center>

    <div id="login-box">

        <h2>Please enter username and password</h2>


        <form method="POST" class="signin" action="j_spring_security_check">

            <table>
                <tr>
                    <td>Username:</td>
                    <td><input type='text' name='j_username' value=''></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type='password' name='j_password' /></td>
                </tr>
                <tr>
                    <td colspan='2'><input name="commit" type="submit"
                                           value="Sign In" /></td>
                </tr>
            </table>
            <c:if test="${param.login_error == 1}">
                <h3>Invalid username or password!</h3>
            </c:if>

            <!--                <input name="commit" type="submit" value="Sign In" />-->

        </form>
    </div>
</body>
</html