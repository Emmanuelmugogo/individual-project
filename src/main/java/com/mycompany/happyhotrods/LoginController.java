/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public String showLoginForm() {
        return "Login";
    }
}
