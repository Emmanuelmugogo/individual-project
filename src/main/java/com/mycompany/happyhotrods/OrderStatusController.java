/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import com.mycompany.happyhotrods.dao.OrderStatusDbDao;
import com.mycompany.happyhotrods.dto.OrderStatus;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class OrderStatusController {
    
    private final OrderStatusDbDao dao;
    
    @Inject
    public OrderStatusController(OrderStatusDbDao dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value="/status/{id}", method = RequestMethod.GET)
    @ResponseBody
    public OrderStatus getOrderStatus(@PathVariable("id") int id){
        return dao.getOrderStatusById(id);
    }
    
    @RequestMapping(value = "/status", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public OrderStatus addOrderStatus(@Valid @RequestBody OrderStatus orderStatus) {
        return dao.addStatus(orderStatus);
    }
    
    @RequestMapping(value = "/status/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderStatus (@PathVariable("id") int id) {
        dao.removeOrderStatus(id);
    }
    
    @RequestMapping(value = "/status/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrderStatus (@PathVariable int id, @RequestBody OrderStatus orderStatus) {
        orderStatus.setStatusId(id);
        dao.updateOrderStatus(orderStatus);
    }
    
    @RequestMapping(value = "/statuses", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderStatus> getAllOrderStatuses() {
        return dao.getAllOrderStatuses();
    }
}
