/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import com.mycompany.happyhotrods.dao.OrderRecordDbDao;
import com.mycompany.happyhotrods.dao.SearchTerm;
import com.mycompany.happyhotrods.dto.OrderRecord;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class OrderRecordController {

    private final OrderRecordDbDao dao;

    @Inject
    public OrderRecordController(OrderRecordDbDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/orderRecord/{id}", method = RequestMethod.GET)
    @ResponseBody
    public OrderRecord getOrderRecord(@PathVariable("id") int id) {
        return dao.getOrderByRecordId(id);
    }

    @RequestMapping(value = "/orderRecord", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public OrderRecord addOrderRecord(@Valid @RequestBody OrderRecord orderRecord) {
        return dao.addOrderRecord(orderRecord);
    }

    @RequestMapping(value = "/orderRecord/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderRecord(@PathVariable("id") int id) {
        dao.removeOrderRecord(id);
    }

    @RequestMapping(value = "/orderRecord/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrderRecord(@PathVariable int id, @RequestBody OrderRecord orderRecord) {
        orderRecord.setRecordId(id);
        dao.updateOrderRecord(orderRecord);
    }

    @RequestMapping(value = "/orderRecords", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderRecord> getAllorderRecords() {
        return dao.getAllOrderRecords();
    }

    @RequestMapping(value = "search/orderRecords", method = RequestMethod.POST)
    @ResponseBody
    public List<OrderRecord> searchOrderRecords(@RequestBody Map<String, String> searchMap) {
        // Create the map of search criteria to send to the DAO
        Map<SearchTerm, String> criteriaMap = new HashMap<>();

        // Determine which search terms have values, translate the String
        // keys into SearchTerm enums, and set the corresponding values
        // appropriately.
        String currentTerm = searchMap.get("orderId");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.WORK_ORDER, currentTerm);
        }
        currentTerm = searchMap.get("vin");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.VIN, currentTerm);
        }
        currentTerm = searchMap.get("firstName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.CUSTOMER, currentTerm);
        }
        currentTerm = searchMap.get("lastName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.WORKER, currentTerm);
        }
        currentTerm = searchMap.get("status");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.STATUS, currentTerm);
        }

        return dao.searchOrderRecords(criteriaMap);
    }
}
