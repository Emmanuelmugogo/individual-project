///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mycompany.happyhotrods;
//
//import com.mycompany.happyhotrods.dao.UserDbDao;
//import com.mycompany.happyhotrods.dto.UserProfile;
//import java.util.List;
//import javax.inject.Inject;
//import javax.validation.Valid;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import com.mycompany.happyhotrods.dao.UserProfileDbDao;
//import com.mycompany.happyhotrods.dto.FullUserProfile;
//import com.mycompany.happyhotrods.dto.User;
//
///**
// *
// * @author Emmanuelmugogo
// */
//@Controller
//public class UserProfileController {
//    
//    private final UserProfileDbDao userProfileDao;
//    private final UserDbDao userDao;
//    
//    @Inject
//    public UserProfileController(UserProfileDbDao userProfileDao, UserDbDao userDao) {
//        this.userProfileDao = userProfileDao;
//        this.userDao = userDao;
//    }
//    
//    @RequestMapping(value="/userProfile/{id}", method = RequestMethod.GET)
//    @ResponseBody
//    public UserProfile getUser(@PathVariable("id") int id){
//        return userProfileDao.getUserById(id);
//    }
//    
//    @RequestMapping(value = "/userProfile", method = RequestMethod.POST)
//    @ResponseStatus(HttpStatus.CREATED)
//    @ResponseBody
//    public UserProfile addUser(@Valid @RequestBody FullUserProfile fullUserProfile) {
//        User user = new User();
//        user.setUserName(fullUserProfile.getUserName());
//        user = userDao.addUser(user);
//        
//        UserProfile userProfile = new UserProfile();
//        userProfile.setProfileId(user.getUserId());
//        userProfile.setFirstName(fullUserProfile.getFirstName());
//        userProfile.setLastName(fullUserProfile.getLastName());
//        userProfile.setPhone(fullUserProfile.getPhone());
//        userProfile.setEmail(fullUserProfile.getEmail());
//        userProfile.setAddress(fullUserProfile.getAddress());
//        
//        
//        return userProfileDao.addUser(userProfile);
//    }
//    
//    @RequestMapping(value = "/userProfile/{id}", method = RequestMethod.DELETE)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteUser(@PathVariable("id") int id) {
//        userProfileDao.removeUser(id);
//    }
//    
//    @RequestMapping(value = "/userProfile/{id}", method = RequestMethod.PUT)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void updateUser(@PathVariable int id, @RequestBody UserProfile userProfile) {
//        userProfile.setProfileId(id);
//        userProfileDao.updateUser(userProfile);
//    }
//    
//    @RequestMapping(value = "/userProfiles", method = RequestMethod.GET)
//    @ResponseBody
//    public List<UserProfile> getAllUsers() {
//        return userProfileDao.getAllUsers();
//    }
//    
//    
//}
