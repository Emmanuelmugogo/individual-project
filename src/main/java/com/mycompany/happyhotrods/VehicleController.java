/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import com.mycompany.happyhotrods.dao.VehicleDbDao;
import com.mycompany.happyhotrods.dto.Vehicle;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class VehicleController {

    private final VehicleDbDao dao;

    @Inject
    public VehicleController(VehicleDbDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/vehicle/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Vehicle getVehicle(@PathVariable("id") int id) {
        return dao.getVehicleById(id);
    }
    
    @RequestMapping(value = "/vehicle/{vin}", method = RequestMethod.GET)
    @ResponseBody
    public Vehicle showVehicleByVin(@PathVariable("vin") String vin) {
        return dao.getVehicleByVin(vin);
    }

    @RequestMapping(value = "/vehicle", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Vehicle addVehicle(@Valid @RequestBody Vehicle vehicle) {
        return dao.addVehicle(vehicle);
    }

    @RequestMapping(value = "/vehicle/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteVehicle(@PathVariable("id") int id) {
        dao.removeVehicle(id);
    }

    @RequestMapping(value = "/vehicle/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateVehicle(@PathVariable int id, @RequestBody Vehicle vehicle) {
        vehicle.setVehicleId(id);
        dao.updateVehicle(vehicle);
    }

    @RequestMapping(value = "/vehicles", method = RequestMethod.GET)
    @ResponseBody
    public List<Vehicle> getAllVehicles() {
        return dao.getAllVehicles();
    }

}
