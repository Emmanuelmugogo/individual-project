/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class HomeController {

    @RequestMapping(value = {"/", "/Home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "Home";
    }
    
    @RequestMapping(value = {"/MainAjaxPage"}, method = RequestMethod.GET)
    public String displayMainAjexPage() {
        return "MainAjaxPage";
    }
    
    @RequestMapping(value = {"/ContactPage"}, method = RequestMethod.GET)
    public String displayContactPage() {
        return "ContactPage";
    }
    
    @RequestMapping(value = "/Contact", method = RequestMethod.GET)
    public String displayContact() {
        return "Contact";
    }
    
    @RequestMapping(value = "/Members", method = RequestMethod.GET)
    public String displayMembersPage() {
        return "Members";
    }
    
    @RequestMapping(value = "/Profile", method = RequestMethod.GET)
    public String displayProfilePage() {
        return "Profile";
    }
    
    @RequestMapping(value = "/Status", method = RequestMethod.GET)
    public String displayStatusPage() {
        return "Status";
    }
    
    @RequestMapping(value = "/WorkOrders", method = RequestMethod.GET)
    public String displayWorkOrdersPage() {
        return "WorkOrders";
    }

    @RequestMapping(value = "/Admin", method = RequestMethod.GET)
    public String displayAdminPage() {
        return "Admin";
    }
    @RequestMapping(value = "/Vehicle", method = RequestMethod.GET)
    public String displayVehiclePage() {
        return "Vehicle";
    }
    @RequestMapping(value = "/Order", method = RequestMethod.GET)
    public String displayOrderPage() {
        return "Order";
    }
}
