/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.OrderRecord;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public class OrderRecordDbDaoImpl implements OrderRecordDbDao {

    private static final String SQL_INSERT_RECORD
            = "insert into order_records (record_date, eta_date, note) values (?,?,?)";

    private static final String SQL_DELETE_RECORD
            = "delete from order_records where record_id = ?";

    private static final String SQL_SELECT_RECORD
            = "select * from order_records where record_id = ?";

    private static final String SQL_UPDATE_RECORD
            = "update order_records set record_date= ?, eta_date= ?, note= ?";

    private static final String SQL_SELECT_ALL_RECORDS
            = "select * from order_records";

    private JdbcTemplate jdbcTemplate;
    private OrderStatusDbDao osDao;
    private UserDbDao uDao;
    private WorkOrderDbDao woDao;
    
    

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public void setOsDao(OrderStatusDbDao osDao) {
        this.osDao = osDao;
    }
    
    public void setUserDbDao(UserDbDao uDao) {
        this.uDao = uDao;
    }
    
    public void setWorkOrderDbDao(WorkOrderDbDao woDao) {
        this.woDao = woDao;
    }
    
//    public OrderRecordDbDaoImpl (WorkOrderDbDao woDao, UserDbDao uDao, OrderStatusDbDao osDao) {
//        this.osDao = osDao;
//        this.uDao = uDao;
//        this.woDao = woDao;
//    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public OrderRecord addOrderRecord(OrderRecord orderRecord) {
        jdbcTemplate.update(SQL_INSERT_RECORD,
                orderRecord.getWorkOrder().getOrderId(),
                orderRecord.getUser().getUserId(),
                orderRecord.getOrderStatus().getStatusId(),
                orderRecord.getRecordDate(),
                orderRecord.getEtaDate(),
                orderRecord.getNotes());

        orderRecord.setRecordId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));

        return orderRecord;
    }

    @Override
    public void removeOrderRecord(int recordId) {
        jdbcTemplate.update(SQL_DELETE_RECORD, recordId);
    }

    @Override
    public void updateOrderRecord(OrderRecord orderRecord) {
        jdbcTemplate.update(SQL_UPDATE_RECORD,
                orderRecord.getWorkOrder().getOrderId(),
                orderRecord.getUser().getUserId(),
                orderRecord.getOrderStatus().getStatusId(),
                orderRecord.getRecordDate(),
                orderRecord.getEtaDate(),
                orderRecord.getNotes(),
                orderRecord.getRecordId());
    }

    @Override
    public List<OrderRecord> getAllOrderRecords() {
        return jdbcTemplate.query(SQL_SELECT_ALL_RECORDS, new OrderRecordMapper());
    }

    @Override
    public OrderRecord getOrderByRecordId(int recordId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_RECORD,
                    new OrderRecordMapper(), recordId);
        } catch (EmptyResultDataAccessException ex) {

            return null;
        }
    }

    @Override
    public List<OrderRecord> searchOrderRecords(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllOrderRecords();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from order_records where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();

            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();

                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new OrderRecordMapper(), paramVals);
        }
    }

    private final class OrderRecordMapper implements RowMapper<OrderRecord> {

        @Override
        public OrderRecord mapRow(ResultSet rs, int i) throws SQLException {

            OrderRecord orderRecord = new OrderRecord();

            orderRecord.setRecordId(rs.getInt("record_id"));
            orderRecord.setWorkOrder(woDao.getWorkOrderById(rs.getInt("order_id")));
            orderRecord.setUser(uDao.getUserById(rs.getInt("employee_id")));
            orderRecord.setOrderStatus(osDao.getOrderStatusById(rs.getInt("status_id")));
            orderRecord.setRecordDate(rs.getString("record_date"));
            orderRecord.setEtaDate(rs.getString("eta_date"));
            orderRecord.setNotes(rs.getString("note"));

            return orderRecord;
        }
    }

}
