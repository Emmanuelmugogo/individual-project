/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.WorkOrder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public class WorkOrderDbDaoImpl implements WorkOrderDbDao {

    private static final String SQL_INSERT_WORKORDER
            = "insert into work_orders (notes) values (?)";

    private static final String SQL_DELETE_WORKORDER
            = "delete from work_orders where order_id = ?";

    private static final String SQL_SELECT_WORKORDER
            = "select * from work_orders where order_id = ?";

    private static final String SQL_UPDATE_WORKORDER
            = "update work_orders set notes = ?";

    private static final String SQL_SELECT_ALL_WORKORDERS
            = "select * from work_orders";

    private JdbcTemplate jdbcTemplate;
    private VehicleDbDao vDao;
    private UserDbDao uDao;
    

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public void setVehicleDbDao(VehicleDbDao vDao) {
        this.vDao = vDao;
    }
    
    public void setUserDbDao(UserDbDao uDao) {
        this.uDao = uDao;
    }

//    public WorkOrderDbDaoImpl(UserDbDao uDao, VehicleDbDao vDao) {
//        this.uDao = uDao;
//        this.vDao = vDao;
//    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public WorkOrder addWorkOrder(WorkOrder workOrder) {
        jdbcTemplate.update(SQL_INSERT_WORKORDER,
                workOrder.getUser().getUserId(),
                workOrder.getVehicle().getVehicleId(),
                workOrder.getNotes());

        workOrder.setOrderId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));

        return workOrder;
    }

    @Override
    public void removeWorkOrder(int orderId) {
        jdbcTemplate.update(SQL_DELETE_WORKORDER, orderId);
    }

    @Override
    public void updateWorkOrder(WorkOrder workOrder) {
        jdbcTemplate.update(SQL_UPDATE_WORKORDER,
                workOrder.getUser().getUserId(),
                workOrder.getVehicle().getVehicleId(),
                workOrder.getNotes(),
                workOrder.getOrderId());
    }

    @Override
    public List<WorkOrder> getAllWorkOrders() {
        return jdbcTemplate.query(SQL_SELECT_ALL_WORKORDERS, new WorkOrderMapper());
    }

    @Override
    public WorkOrder getWorkOrderById(int orderId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_WORKORDER,
                    new WorkOrderMapper(), orderId);
        } catch (EmptyResultDataAccessException ex) {
            // there were no results for the given order Id - we just want to
            // return null in this case
            return null;
        }
    }

    @Override
    public List<WorkOrder> searchWorkOrders(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllWorkOrders();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from work_orders where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();

            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();

                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new WorkOrderMapper(), paramVals);
        }
    }

    private final class WorkOrderMapper implements RowMapper<WorkOrder> {

        @Override
        public WorkOrder mapRow(ResultSet rs, int i) throws SQLException {

            WorkOrder workOrder = new WorkOrder();

            workOrder.setOrderId(rs.getInt("order_id"));
            workOrder.setUser(uDao.getUserById(rs.getInt("customer_id")));
            workOrder.setVehicle(vDao.getVehicleById(rs.getInt("vehicle_id")));
            workOrder.setNotes(rs.getString("notes"));
            
            return workOrder;
        }
    }

}
