/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.OrderStatus;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emmanuelmugogo
 */
public interface OrderStatusDbDao {
    
    public OrderStatus addStatus (OrderStatus orderStatus);
    
    public void removeOrderStatus (int orderId);
    
    public void updateOrderStatus (OrderStatus orderStatus);
    
    public List<OrderStatus> getAllOrderStatuses ();
    
    public OrderStatus getOrderStatusById (int orderId);
    
    public List<OrderStatus> searchOrderStatus (Map<SearchTerm, String> criteria);
}
