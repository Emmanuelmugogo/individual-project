/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

/**
 *
 * @author Emmanuelmugogo
 */
public enum Status {
    NEW,
    AWAITING_EVALUATION,
    AWAITING_APPROVAL,
    WAITING_FOR_PARTS,
    IN_REPAIRS,
    AWAITING_PICKUP,
    CLOSED;
}
