/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.Authority;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emmanuelmugogo
 */
public interface AuthorityDbDao {
    public Authority addAuthority (Authority authority);
    
    public void updateAuthority (Authority authority);
    
    public void removeAuthority (String userName);
    
    public Authority getAuthorityByUserName (String userName);
    
    public List<Authority> getAllAuthorities ();
    
    
    public List<Authority> searchAuthorities (Map<SearchTerm, String> criteria);
}
