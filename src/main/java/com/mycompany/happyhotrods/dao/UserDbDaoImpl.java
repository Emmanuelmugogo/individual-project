/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public class UserDbDaoImpl implements UserDbDao {

    private static final String SQL_INSERT_USER
            = "insert into users (username, password, first_name, last_name, email, phone, address, enabled) value (?,?,?,?,?,?,?,?)";
    private static final String SQL_DELETE_USER
            = "delete from users where user_Id = ?";
    private static final String SQL_UPDATE_USER
            = "update users set username = ?, password = ?, first_name =?, last_name = ?, email = ?, phone = ?, address = ?,enabled = ? where user_Id = ?";
    private static final String SQL_RESET_PASSWORD
            = "update users set password = 'happy-hotrods' where user_Id = ?";
    private static final String SQL_SELECT_ALL_USERS
            = "select * from users";
//    private static final String SQL_SELECT_USER_ROLES
//            = "select users.*, authorities.authority from users join authorities on authorities.username = users.username where user_Id = ?";
    private static final String SQL_SELECT_USER
            = "select * from users where user_Id = ?";
    
    private JdbcTemplate jdbcTemplate;
    private AuthorityDbDao aDao;

    @Override
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        
    }
    
    public void setADao(AuthorityDbDao aDao) {   //setter Injection
        this.aDao = aDao;
        
    }
    
//    public UserDbDaoImpl (AuthorityDbDao aDao) { //Contructor injection
//        this.aDao = aDao;
//    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public User addUser(User user) {
        jdbcTemplate.update(SQL_INSERT_USER,
                user.getUserName(),
                user.getPassword(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPhone(),
                user.getAddress(),
                1);
        user.setUserId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return user;
    }

    @Override
    public void removeUser(int userId) {
        jdbcTemplate.update(SQL_DELETE_USER, userId);
    }

    @Override
    public void updateUser(User user) {
        jdbcTemplate.update(SQL_UPDATE_USER,
                user.getUserName(),
                user.getPassword(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPhone(),
                user.getAddress(),
                1,
                user.getUserId());
    }

    @Override
    public void resetPassword(int userId) {
        jdbcTemplate.update(SQL_RESET_PASSWORD, userId);
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SQL_SELECT_ALL_USERS, new UserMapper());
    }

    @Override
    public User getUserById(int userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_USER, new UserMapper(), userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<User> searchUsers(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllUsers();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from users where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();

            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();

                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new UserMapper(), paramVals);
        }
    }

    @Override
    public User getUserByUserName(String userName) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_USER, new UserMapper(), userName);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private final class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setUserId(rs.getInt("user_Id"));
            user.setUserName(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setEmail(rs.getString("email"));
            user.setPhone(rs.getString("phone"));
            user.setAddress(rs.getString("address"));
//            user.setAuthority(aDao.getAuthorityByUserName(rs.getString("authority")));

            return user;
        }

    }
}
