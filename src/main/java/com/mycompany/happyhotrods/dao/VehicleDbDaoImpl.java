/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.Vehicle;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public class VehicleDbDaoImpl implements VehicleDbDao {

    private static final String SQL_INSERT_VEHICLE
            = "insert into vehicles (vin, make, model, year, color, notes) values (?,?,?,?,?,?)";

    private static final String SQL_DELETE_VEHICLE
            = "delete from vehicles where vehicle_id = ?";

    private static final String SQL_SELECT_VEHICLE
            = "select * from vehicles where vehicle_id = ?";
    
    private static final String SQL_SELECT_VEHICLE_BY_VIN
            = "select * from vehicles where vin = ?";

    private static final String SQL_UPDATE_VEHICLE
            = "update vehicles set vin = ?, make = ?, model = ?, year = ?, color = ?, notes = ?";

    private static final String SQL_SELECT_ALL_VEHICLES
            = "select * from vehicles";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Vehicle addVehicle(Vehicle vehicle) {
        jdbcTemplate.update(SQL_INSERT_VEHICLE,
                vehicle.getVin(),
                vehicle.getMake(),
                vehicle.getModel(),
                vehicle.getYear(),
                vehicle.getColor(),
                vehicle.getNotes());

        vehicle.setVehicleId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));

        return vehicle;

    }

    @Override
    public void removeVehicle(int vehicleId) {
        jdbcTemplate.update(SQL_DELETE_VEHICLE, vehicleId);
    }

    @Override
    public void updateVehicle(Vehicle vehicle) {
        jdbcTemplate.update(SQL_UPDATE_VEHICLE,
                vehicle.getVin(),
                vehicle.getMake(),
                vehicle.getModel(),
                vehicle.getYear(),
                vehicle.getColor(),
                vehicle.getNotes(),
                vehicle.getVehicleId());
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        return jdbcTemplate.query(SQL_SELECT_ALL_VEHICLES, new VehicleMapper());
    }

    @Override
    public Vehicle getVehicleById(int vehicleId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_VEHICLE,
                    new VehicleMapper(), vehicleId);
        } catch (EmptyResultDataAccessException ex) {
            // there were no results for the given vehicle id - we just want to
            // return null in this case
            return null;
        }
    }

    @Override
    public List<Vehicle> searchUsers(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllVehicles();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from vehicles where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();

            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();

                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new VehicleDbDaoImpl.VehicleMapper(), paramVals);
        }
    }

    @Override
    public Vehicle getVehicleByVin(String vin) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_VEHICLE_BY_VIN,
                    new VehicleMapper(), vin);
        } catch (EmptyResultDataAccessException ex) {
            // there were no results for the given vin - we just want to
            // return null in this case
            return null;
        }
    }

    private static final class VehicleMapper implements RowMapper<Vehicle> {

        @Override
        public Vehicle mapRow(ResultSet rs, int i) throws SQLException {

            Vehicle vehicle = new Vehicle();
            vehicle.setVehicleId(rs.getInt("vehicle_id"));
            vehicle.setVin(rs.getString("vin"));
            vehicle.setMake(rs.getString("make"));
            vehicle.setModel(rs.getString("model"));
            vehicle.setYear(rs.getInt("year"));
            vehicle.setColor(rs.getString("color"));
            vehicle.setNotes(rs.getString("notes"));

            return vehicle;
        }
    }

}
