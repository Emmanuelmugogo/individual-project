/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.User;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public interface UserDbDao {
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    User addUser(User user);

    List<User> getAllUsers();

    User getUserById(int userId);
    
    User getUserByUserName (String userName);

    void removeUser(int userId);

    void setJdbcTemplate(JdbcTemplate jdbcTemplate);

    void updateUser(User user);
    
    void resetPassword(int userId);
    
    public List<User> searchUsers(Map<SearchTerm, String> criteria);

//    public List<String> getRolesForUser(int userId);
}
