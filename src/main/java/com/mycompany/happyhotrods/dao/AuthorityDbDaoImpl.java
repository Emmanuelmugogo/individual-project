/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.Authority;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public class AuthorityDbDaoImpl implements AuthorityDbDao {

    private static final String SQL_INSERT_AUTHORITY
            = "insert into authorities (username, authority) values (?,?)";

    private static final String SQL_UPDATE_AUTHORITY
            = "update authorities set username = ?, authority = ?";
    
    private static final String SQL_DELETE_AUTHORITY
            = "delete from authorities where username = ?";

    private static final String SQL_SELECT_AUTHORITY
            = "select * from authorities where username = ?";

    private static final String SQL_SELECT_AUTHORITIES
            = "select * from authorities";

    private JdbcTemplate jdbcTemplate;
    private UserDbDao uDao;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public void setUserDbDao(UserDbDao uDao) {
        this.uDao = uDao;
    }
    
//    public AuthorityDbDaoImpl (UserDbDao uDao) {
//        this.uDao = uDao;
//    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Authority addAuthority(Authority authority) {
        jdbcTemplate.update(SQL_INSERT_AUTHORITY,
                authority.getUser().getUserName(),
                authority.getSiteRole());

        return authority;
    }

    @Override
    public void updateAuthority(Authority authority) {
        jdbcTemplate.update(SQL_UPDATE_AUTHORITY,
                authority.getUser().getUserName(),
                authority.getSiteRole());
    }

    @Override
    public List<Authority> getAllAuthorities() {
        return jdbcTemplate.query(SQL_SELECT_AUTHORITIES, new AuthorityMapper());
    }

    @Override
    public Authority getAuthorityByUserName(String userName) {
try {
            return jdbcTemplate.queryForObject(SQL_SELECT_AUTHORITY,
                    new AuthorityMapper(), userName);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }   
    }

    @Override
    public void removeAuthority(String userName) {
        jdbcTemplate.update(SQL_DELETE_AUTHORITY, userName);
    }
    
    @Override
public List<Authority> searchAuthorities(Map<SearchTerm, String> criteria){
        if (criteria.size() == 0) {
            return getAllAuthorities();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from authorities where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();

            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();

                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new AuthorityMapper(), paramVals);
        }
}

//    @Override
//    public List<Authority> searchAuthorities(Map<SearchTerm, String> criteria) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    private final class AuthorityMapper implements RowMapper<Authority> {

        @Override
        public Authority mapRow(ResultSet rs, int i) throws SQLException {
            
            Authority authority = new Authority();
            authority.setUser(uDao.getUserByUserName(rs.getString("username")));
            authority.setSiteRole(rs.getString("authority"));
            
            
            return authority;
        }
    }

}
