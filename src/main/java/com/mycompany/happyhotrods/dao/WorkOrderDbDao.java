/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.WorkOrder;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emmanuelmugogo
 */
public interface WorkOrderDbDao {
    
    public WorkOrder addWorkOrder (WorkOrder workOrder);
    
    public void removeWorkOrder (int orderId);
    
    public void updateWorkOrder (WorkOrder workOrder);
    
    public List<WorkOrder> getAllWorkOrders ();
    
    public WorkOrder getWorkOrderById (int orderId);
    
    public List<WorkOrder> searchWorkOrders (Map<SearchTerm, String> criteria);
}
