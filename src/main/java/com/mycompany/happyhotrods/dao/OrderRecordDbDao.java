/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.OrderRecord;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emmanuelmugogo
 */
public interface OrderRecordDbDao {
    
    public OrderRecord addOrderRecord (OrderRecord orderRecord);
    
    public void removeOrderRecord (int recordId);
    
    public void updateOrderRecord (OrderRecord orderRecord);
    
    public List<OrderRecord> getAllOrderRecords ();
    
    public OrderRecord getOrderByRecordId (int recordId);
    
    public List<OrderRecord> searchOrderRecords (Map<SearchTerm, String> criteria);
    
}
