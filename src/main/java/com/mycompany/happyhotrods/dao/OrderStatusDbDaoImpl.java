/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.OrderStatus;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emmanuelmugogo
 */
public class OrderStatusDbDaoImpl implements OrderStatusDbDao {

    private static final String SQL_INSERT_STATUS
            = "insert into order_statuses (status) values (?)";

    private static final String SQL_DELETE_STATUS
            = "delete from order_statuses where status_id = ?";

    private static final String SQL_SELECT_STATUS
            = "select * from order_statuses where status_id = ?";

    private static final String SQL_UPDATE_STATUS
            = "update order_statuses set status = ?";

    private static final String SQL_SELECT_ALL_STATUSES
            = "select * from order_statuses";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public OrderStatus addStatus(OrderStatus orderStatus) {
        jdbcTemplate.update(SQL_INSERT_STATUS, orderStatus.getStatus());

        orderStatus.setStatusId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));

        return orderStatus;
    }

    @Override
    public void removeOrderStatus(int orderId) {
        jdbcTemplate.update(SQL_DELETE_STATUS, orderId);
    }

    @Override
    public void updateOrderStatus(OrderStatus orderStatus) {
        jdbcTemplate.update(SQL_UPDATE_STATUS,
                orderStatus.getStatus(),
                orderStatus.getStatusId());
    }

    @Override
    public List<OrderStatus> getAllOrderStatuses() {
        return jdbcTemplate.query(SQL_SELECT_ALL_STATUSES, new StatusMapper());
    }

    @Override
    public OrderStatus getOrderStatusById(int orderId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_STATUS,
                    new StatusMapper(), orderId);
        } catch (EmptyResultDataAccessException ex) {
            // there were no results for the given order id - we just want to
            // return null in this case
            return null;
        }
    }

    @Override
    public List<OrderStatus> searchOrderStatus(Map<SearchTerm, String> criteria) {
 if (criteria.size() == 0) {
            return getAllOrderStatuses();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from order_statuses where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();
            
            while(iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                
                if(paramPosition > 0) {
                    sQuery.append(" and ");
                }
                
                sQuery.append(currentKey);
                sQuery.append(" = ? ");
                
                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;    
            }
            return jdbcTemplate.query(sQuery.toString(), new StatusMapper(), paramVals);
        }    }

    private static final class StatusMapper implements RowMapper<OrderStatus> {

        @Override
        public OrderStatus mapRow(ResultSet rs, int i) throws SQLException {
            
            OrderStatus orderStatus = new OrderStatus();
            orderStatus.setStatusId(rs.getInt("status_id"));
            orderStatus.setStatus(rs.getString("status"));
            
            return orderStatus;
        }
    }

}
