/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dao;

import com.mycompany.happyhotrods.dto.Vehicle;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emmanuelmugogo
 */
public interface VehicleDbDao {
    
    public Vehicle addVehicle (Vehicle vehicle);
    
    public void removeVehicle (int vehicleId);
    
    public void updateVehicle (Vehicle vehicle);
    
    public List<Vehicle> getAllVehicles ();
    
    public Vehicle getVehicleById (int vehicleId);
    
    public Vehicle getVehicleByVin (String vin);
        
    public List<Vehicle> searchUsers (Map<SearchTerm, String> criteria);
    
}
