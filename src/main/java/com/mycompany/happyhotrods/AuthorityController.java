/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import com.mycompany.happyhotrods.dao.AuthorityDbDao;
import com.mycompany.happyhotrods.dao.SearchTerm;
import com.mycompany.happyhotrods.dto.Authority;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class AuthorityController {
    
    private final AuthorityDbDao authorityDbDao;
    
    @Inject
    public AuthorityController(AuthorityDbDao authorityDbDao){
        this.authorityDbDao = authorityDbDao;
    }
    
    @RequestMapping(value="/authority/{userName}", method = RequestMethod.GET)
    @ResponseBody
    public Authority getAuthority(@PathVariable("userName") String userName){
        return authorityDbDao.getAuthorityByUserName(userName);
    }
    
    @RequestMapping(value = "/authority", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Authority addAuthority (@Valid @RequestBody Authority authority) {
        return authorityDbDao.addAuthority(authority);
    }
    
    @RequestMapping(value = "/authority/{userName}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAuthority(@PathVariable("userName") String userName) {
        authorityDbDao.removeAuthority(userName);
    }
    
    @RequestMapping(value = "/authority/{userName}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAuthority(@PathVariable int id, @RequestBody Authority authority) {
        
        authorityDbDao.updateAuthority(authority);
    }
    
    @RequestMapping(value = "/authorities", method = RequestMethod.GET)
    @ResponseBody
    public List<Authority> getAllAuthorities() {

        return authorityDbDao.getAllAuthorities();

    }
    
    
    @RequestMapping(value = "search/authorities", method = RequestMethod.POST)
    @ResponseBody
    public List<Authority> searchAuthorities(@RequestBody Map<String, String> searchMap) {
        // Create the map of search criteria to send to the DAO
        Map<SearchTerm, String> criteriaMap = new HashMap<>();

        // Determine which search terms have values, translate the String
        // keys into SearchTerm enums, and set the corresponding values
        // appropriately.
        String currentTerm = searchMap.get("userId");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.USER_ID, currentTerm);
        }
        currentTerm = searchMap.get("userName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.NAME, currentTerm);
        }
        

        return authorityDbDao.searchAuthorities(criteriaMap);
    }
    
    
}
