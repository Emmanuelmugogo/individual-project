/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods.dto;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Emmanuelmugogo
 */
public class OrderRecord {
    private int recordId;
    private WorkOrder workOrder;
    private User user;
    private OrderStatus orderStatus;
    private String recordDate;
    private String etaDate;
    private String notes;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.recordId;
        hash = 89 * hash + Objects.hashCode(this.workOrder);
        hash = 89 * hash + Objects.hashCode(this.user);
        hash = 89 * hash + Objects.hashCode(this.orderStatus);
        hash = 89 * hash + Objects.hashCode(this.recordDate);
        hash = 89 * hash + Objects.hashCode(this.etaDate);
        hash = 89 * hash + Objects.hashCode(this.notes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderRecord other = (OrderRecord) obj;
        if (this.recordId != other.recordId) {
            return false;
        }
        if (!Objects.equals(this.recordDate, other.recordDate)) {
            return false;
        }
        if (!Objects.equals(this.etaDate, other.etaDate)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.workOrder, other.workOrder)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.orderStatus, other.orderStatus)) {
            return false;
        }
        return true;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public String getEtaDate() {
        return etaDate;
    }

    public void setEtaDate(String etaDate) {
        this.etaDate = etaDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
