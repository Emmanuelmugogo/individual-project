/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import com.mycompany.happyhotrods.dao.WorkOrderDbDao;
import com.mycompany.happyhotrods.dto.WorkOrder;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emmanuelmugogo
 */
@Controller
public class WorkOrderController {

    private final WorkOrderDbDao dao;

    @Inject
    public WorkOrderController(WorkOrderDbDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/workOrder/{id}", method = RequestMethod.GET)
    @ResponseBody
    public WorkOrder getWorkOrder(@PathVariable("id") int id) {
        return dao.getWorkOrderById(id);
    }

    @RequestMapping(value = "/workOrder", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public WorkOrder addWorkOrder(@Valid @RequestBody WorkOrder workOrder) {
        return dao.addWorkOrder(workOrder);
    }

    @RequestMapping(value = "/workOrder/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteWorkOrder(@PathVariable("id") int id) {
        dao.removeWorkOrder(id);
    }

    @RequestMapping(value = "/workOrder/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateWorkOrder(@PathVariable int id, @RequestBody WorkOrder workOrder) {
        workOrder.setOrderId(id);
        dao.updateWorkOrder(workOrder);
    }

    @RequestMapping(value = "/workOrders", method = RequestMethod.GET)
    @ResponseBody
    public List<WorkOrder> getAllWorkOrders() {
        return dao.getAllWorkOrders();
    }
}
