/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.happyhotrods;

import com.mycompany.happyhotrods.dao.SearchTerm;
import com.mycompany.happyhotrods.dao.UserDbDao;
import com.mycompany.happyhotrods.dto.User;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emmanuelmugogo
 */

@Controller
public class UserController {
    
    private final UserDbDao userDbDao;

    @Inject
    public UserController(UserDbDao userDbDao) {

        this.userDbDao = userDbDao;

    }
    
    @RequestMapping(value="/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable("id") int id){
        return userDbDao.getUserById(id);
    }
    
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public User addUser(@Valid @RequestBody User user) {
        return userDbDao.addUser(user);
    }
    
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("id") int id) {
        userDbDao.removeUser(id);
    }
    
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateUser(@PathVariable int id, @RequestBody User user) {
        user.setUserId(id);
        userDbDao.updateUser(user);
    }
    
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getAllUsers() {

        return userDbDao.getAllUsers();

    }
    
    @RequestMapping(value = "search/users", method = RequestMethod.POST)
    @ResponseBody
    public List<User> searchUsers(@RequestBody Map<String, String> searchMap) {
        // Create the map of search criteria to send to the DAO
        Map<SearchTerm, String> criteriaMap = new HashMap<>();

        // Determine which search terms have values, translate the String
        // keys into SearchTerm enums, and set the corresponding values
        // appropriately.
        String currentTerm = searchMap.get("userId");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.USER_ID, currentTerm);
        }
        currentTerm = searchMap.get("userName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.NAME, currentTerm);
        }
        

        return userDbDao.searchUsers(criteriaMap);
    }
    
}
