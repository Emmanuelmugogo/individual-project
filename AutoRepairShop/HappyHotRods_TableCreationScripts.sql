order_recordsDROP SCHEMA IF EXISTS `HappyHotrods`;
CREATE SCHEMA `HappyHotrods` ;
USE `HappyHotrods`;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `users`
--
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
 `user_id` int(11) NOT NULL AUTO_INCREMENT,
 `username` varchar(70) NOT NULL UNIQUE,
 `password` varchar(20) NOT NULL,
 `enabled` tinyint(1) NOT NULL,
 PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;
--
-- Dumping data for table `users`
--
INSERT INTO `users` (`user_id`, `username`, `password`, `enabled`) VALUES
	(1, "test_admin", "password", 1),
	(2, "test_employee", "password", 1),
	(3, "test_user1", "password", 1),
	(4, "test_user2", "password", 1);
--
-- Table structure for table `authorities`
--
CREATE TABLE IF NOT EXISTS `authorities` (
 `username` varchar(70) NOT NULL,
 `authority` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Dumping data for table `authorities`
--
INSERT INTO `authorities` (`username`, `authority`) VALUES
("test_admin", "ROLE_ADMIN"),
("test_admin", "ROLE_EMPLOYEE"),
("test_admin", "ROLE_USER"),
("test_employee", "ROLE_EMPLOYEE"),
("test_employee", "ROLE_USER"),
("test_user1", "ROLE_USER"),
("test_user2", "ROLE_USER");
--
-- Constraints for table `authorities`
--
ALTER TABLE `authorities`
 ADD CONSTRAINT `authorities_fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Table structure for table `user_profiles`
--
CREATE TABLE IF NOT EXISTS `user_profiles` (
	`profile_id` int(11) NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(20) NOT NULL,
	`last_name` VARCHAR(40) NOT NULL,
	`email` VARCHAR(70) NOT NULL,
	`phone` VARCHAR(20) NOT NULL,
	`address` VARCHAR(250) NOT NULL,
 	PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Dumping data for table `user_profiles`
--
INSERT INTO `user_profiles` (`profile_id`, `first_name`, `last_name`, `email`, `phone`, `address`) VALUES
( 1, "TEST" , "ADMIN", "admin@happyhotrods.com", "555-555-TEST","101 admin way, quionoa ms 40205"),
( 2, "TEST" , "EMPLOYEE", "employee@happyhotrods.com", "555-555-TEST","101 employee way, kimchee wa 90852"),
( 3, "TEST" , "USER1", "user1@gmail.com", "555-555-TEST", "101 user way, mayo tx 87666"),
( 4, "TEST" , "USER2", "user2@gmail.com", "555-555-TEST", "201 user way, mayo tx 87666");

--
-- Table structure for table `user_profile_mapping`
--
CREATE TABLE IF NOT EXISTS `user_profile_mapping` (
	`user_id` int(11) NOT NULL,
	`profile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Constraints for table `user_profile_mapping`
--
ALTER TABLE `user_profile_mapping`
 ADD CONSTRAINT `user_profile_mapping_fk_userid` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
 ADD CONSTRAINT `user_profile_mapping_fk_profileid` FOREIGN KEY (`profile_id`) REFERENCES `user_profiles` (`profile_id`);

--
-- Dumping data for table `authorities`
--
INSERT INTO `user_profile_mapping` (`user_id`, `profile_id`) VALUES
( 1, 1 ),
( 2, 2 ),
( 3, 3 );

--
-- Table structure for table `vehicles`
--
CREATE TABLE IF NOT EXISTS `vehicles` (
	`vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
	`vin` VARCHAR(20) NOT NULL UNIQUE,
	`make` VARCHAR(50) NOT NULL,
	`model` VARCHAR(50) NOT NULL,
	`year` INT(4) NOT NULL,
	`color` VARCHAR(50) NOT NULL,
	`notes` VARCHAR(250) NOT NULL,
	PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


--
-- Dumping data for table `vehicles`
--
INSERT INTO `vehicles` ( `vehicle_id` , `vin` , `make` , `model` , `year` , `color` , `notes` ) VALUES
	( 1, "2HGEJ6423XH006075", "Volkswagen", "New Beetle", "2005", "PolkaDot Red", "Bought used. Some transmission trouble."),
	( 2, "JH2SC6150AK065490", "Serenity", "Firefly", "2301", "Rusted Chrome", "Don't look behind the panels.");


--
-- Table structure for table `work_orders`
--
CREATE TABLE IF NOT EXISTS `work_orders` (
	`order_id` int(11) NOT NULL AUTO_INCREMENT,
	`customer_id` int(11) NOT NULL,
	`vehicle_id` int(11) NOT NULL,
	`notes` VARCHAR(250) NOT NULL,
	PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Constraints for table `work_orders`
--
ALTER TABLE `work_orders`
 ADD CONSTRAINT `workorders_fk_customerid` FOREIGN KEY (`customer_id`) REFERENCES `user_profiles` (`profile_id`),
 ADD CONSTRAINT `workorders_fk_vehicleid` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`);

--
-- Dumping data for table `work_orders`
--

INSERT INTO `work_orders` ( `order_id` , `customer_id` , `vehicle_id` , `notes` ) VALUES
	( 1, 3, 1, "Issue with transmission."),
	( 2, 4, 2, "Diagnostic Checkup.")	;

--
-- Table structure for table `order_statuses`
--
CREATE TABLE IF NOT EXISTS `order_statuses` (
	`status_id` int(11) NOT NULL AUTO_INCREMENT,
	`status` VARCHAR(20) NOT NULL,
	PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

--
-- Dumping data for table `order_statuses`
--
INSERT INTO `order_statuses` (`status_id`, `status`) VALUES
(1, "NEW"),
(2, "AWAITING EVALUATION"),
(3, "UNDER EVALUATION"),
(4, "AWAITING C. APPROVAL"),
(5, "WAITING FOR PARTS"),
(6, "IN REPAIRS"),
(7, "AWAITING PICKUP"),
(8, "CLOSED");

--
-- Dumping data for table `order_records`
--

CREATE TABLE IF NOT EXISTS `order_records` (
	`record_id` int(11) NOT NULL AUTO_INCREMENT,
	`order_id` int(11) NOT NULL,
	`employee_id` int(11) NOT NULL,
	`status_id` int(11) NOT NULL,
	`record_date` DATE NOT NULL,
	`eta_date` VARCHAR(25),	
	`note` VARCHAR(500) NOT NULL,
	PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
--
-- Constraints for table `order_records`
--
ALTER TABLE `order_records`
 ADD CONSTRAINT `orderrecords_fk_orders` FOREIGN KEY (`order_id`) REFERENCES `work_orders` (`order_id`),
 ADD CONSTRAINT `orderrecords_fk_employees` FOREIGN KEY (`employee_id`) REFERENCES `user_profiles` (`profile_id`),
 ADD CONSTRAINT `orderrecords_fk_statuses` FOREIGN KEY (`status_id`) REFERENCES `order_statuses` (`status_id`);

--
-- Dumping data for table `order_records`
--

INSERT INTO `order_records` ( `record_id` , `order_id`, `employee_id` , `status_id` , `eta_date`, `record_date`, `note` ) VALUES
	( 1, 1, 1, 1, "2004-02-02 09:00", "n/a", "Customer reported issues with transmission starting about a week ago."),
	( 2, 1, 1, 2, "2004-02-02 10:00", "n/a","Assigned to test mechanic for evaluation."),
	( 3, 1, 2, 3, "2004-02-03 09:00", "n/a","Investigating transmission and potential wheel issues.."),
	( 4, 1, 2, 4, "2004-02-03 11:00", "2/15/2004","Wheel fine, but transmission repair looks like ~ week long wait for parts. Awaiting customer approval for order."),
	( 5, 1, 1, 5, "2004-02-03 15:00", "2/15/2004","Approval granted. Ordered parts."),
	( 6, 1, 2, 6, "2004-02-10 09:00", "2/12/2004","Beginning transmission repairs."),
	( 7, 1, 2, 7, "2004-02-12 14:00", "2/12/2004","Repairs finished. 20 hrs of work + parts."),
	( 9, 2, 1, 1, "2004-02-15 09:00", "n/a", "Customer wants full diagnostic. No reported issue.");
	
--
-- Dumping data for table `order_assignments`
--

CREATE TABLE IF NOT EXISTS `order_assignments` (
	`order_id` int(11) NOT NULL,
	`assigned_employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
--
-- Constraints for table `order_assignments`
--
ALTER TABLE `order_assignments`
 ADD CONSTRAINT `orderassignments_fk_orders` FOREIGN KEY (`order_id`) REFERENCES `work_orders` (`order_id`),
 ADD CONSTRAINT `orderassignments_fk_employees` FOREIGN KEY (`assigned_employee_id`) REFERENCES `user_profiles` (`profile_id`);

--
-- Dumping data for table `order_assignments`
--

INSERT INTO `order_assignments` ( `order_id`, `assigned_employee_id` ) VALUES
	( 1, 2) ;