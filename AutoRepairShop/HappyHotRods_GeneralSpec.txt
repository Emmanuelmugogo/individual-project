You have been hired to engineer a work order tracking system for Happy HotRods, a local automotive repair shop. Happy wants to be able to provide its customers with a way to keep track of current and past repair visits to their shop, as well as provide their workers with some much needed organization of work orders. 

The general purpose of the site is to allow users and employees to view the status of current and past work orders, as well as employees to track and update current work orders, and admins to add and remove orders, users, and vehicles. 


Project Requirements:
- Documentation
o Daily ?StandUp? Updates (What did, What doing, What blocking)
o Iteration plan (at least 3 iterations over 2 week period)
* Assign User Stories
* Features
o Site Diagram
* JSP Pages
* Endpoints
o UML Diagram
* Controllers
* DTOs
* DAO
* Prepared SQL Statements
* Methods & Properties
- Features
o General purpose
o User Stories
o Wire Frames
- Testing
o All DAO methods MUST have an associated jUnit test.
 

The user stories are as follows:

1. A visitor to the site navigates to the main page, and sees the general Happy HotRod store location, general information & mission statement (and some snazzy examples of past ?best work?)
a. Also available is a menu bar for the home (above) and another for the general contact information, as well as a username/password input, w/ a button to log in to the site. Once logged in, this button becomes logout. 
b. Clicking on the menu bar to the contact link, they are taken to a page that lists the general contact information.

2. A customer member navigates to the main page, and sees the main page w/ the menu bar for the home and contact link, and the username/password inputs w/ button to log in. They correctly submit their username & password, and hit the login button. The page refreshes, and the main page has an updated menu bar that includes a history & status page, a profile link, and the login inputs have been replaced by a logout link.
a. Upon successful login, the profile link in the menu bar directs to a page that shows the users name, email, phone and address. 
i. There is also a form to update the profile information.
1. Information must be complete in form to successfully update in the database.
2. Empty or incorrect information will display errors upon unsuccessful update attempt.
ii. There is also a form to update the password, the passwords must match.
b. Upon successful login, the history & status page link in the menu bar directs to a page that shows the users current open work orders and their latest ETA of completion, as well as a past history of all work orders associated with their customer id.
i. The VIN numbers in the work order tables are clickable and direct to a vehicle history page that shows all logged work orders associated with that vehicle.
ii. The order ids in the work id are clickable and direct to a more detailed work order history that details all of the work order records as individual line items. 
c. Upon successful login, the logout link will log out the user, and redirect to the home page. 
3. Upon successful login, an employee will see their home page refresh with a menu bar that includes Home, Contact, Work Orders, Profile.
a. The Work Orders page upon first click, displays a list of all open work orders (i.e. not closed) and their current assigned employee and status. 
i. Inputting a value into the Search and hitting search, will filter the displayed assets by the inputs given. 
1. The search form allows partial criteria, and will ignore empty fields as a potential criteria filter on results.
2. Search will include ALL work orders, not just open/unclosed orders
ii. The VIN numbers in the work order tables are clickable and direct to a vehicle history page that shows all logged work orders associated with that vehicle.
iii. The order ids in the work id are clickable and direct to a more detailed work order history that details all of the work order records as individual line items. 
iv. Clicking on Update will toggle a modal, which will allow the status of that work order to be updated. 
1. The available statuses are stored in the database.

4. Upon successful login, an admin will see their home page refresh with a menu bar that includes Home, Contact, Work Orders, Members, Admin, Profile
a. The Members page displays a list of all members
i. Inputting a value into the Search and hitting search, will filter the displayed members by the inputs given. 
ii. Inputting values into the Add User form, and clicking the add user button, will add the user into the database if all information is correct, and display in the directory list on the page. The add member form will also be cleared. 
1. If the employee toggle is clicked, the user will be added with employee credentials as well as user credentials.
2. If the admin toggle is clicked, the user will be added with admin credentials as well as user credentials.
3. The default password will be "happy-hotrods!?
4. Information must be complete in form to successfully update in the database.
a. The username must be unique.
Empty or incorrect information will the proper errors will be displayed next to the field. 5. 
6. If the information is corrected and resubmitted, it will properly update the database/display with the new user.
iii. Clicking delete on a member row will prompt the user, and if accepted, will delete that individual from the users and authorities tables, but keep their associated profile for historical records.
iv. Clicking the Reset Password link on a user row will reset that user?s password to be "happy-hotrods!?

b. The Admin page allows the creation of a new work order. 
Inputting values into the Create Order form, and clicking the create order button, will create a new work order with a new order record status of ?new? into the database if all other information is correct. The create order will also be cleared.  
i. To create a new work order the admin must know the user id and the vin number.
1. If the vin number has not previously been tracked in the system, it will not allow the work order to be created and should display an error ? unless the ?new vehicle? toggle is clicked, and the extra vehicle information is filled out.
a. If the new vehicle information is filled out, the vehicle should be added to the database, and then used to create the new work order. 
ii. If the information is not correct, the proper errors will be displayed next to the field.
iii. If the information is corrected and resubmitted, it will properly update the database, and navigating back to the ?work orders? tab would have it displayed in the open orders.
c. The Vehicle History page displays the general car information logged in the database (make, model, color etc.) as well as a tabled list of all work order ids logged against that Vehicle?s VIN#.
i. The order ids in the work id are clickable and direct to a more detailed work order history that details all of the work order records as individual line items. 
ii. The Vehicle History page is only viewable via links in the respective tables, and is not part of the general navigation menu.
d. The Work Order Log page displays the general customer & car information associated with the vehicle & customer id associated with that work order in the database (make, model, color for vehicles, name, phone, email & address for customers etc.) as well as a tabled list of all work order records ids logged against that Work Order?s Id.
i. The Vehicle History page is only viewable via links in the respective tables, and is not part of the general navigation menu.













